import 'bootstrap/dist/css/bootstrap.css'
import './css/style.scss'

import { navigation } from './js/variabelen'
import { showElement } from './js/contentSwitcher'
import { /*showTable,*/ searchInTable } from './js/components/createTable'
import { createAttractiePark } from './js/components/createAttractionParc'
import {toggleVisibility} from './js/contentSwitcher'
import {makeCards} from './js/components/indexPage'

addEventListener()
makeCards()
//showTable()

function addEventListener() {
    console.log(navigation)
    for (let navigationItem of navigation) {
        console.log(navigationItem)
        navigationItem.addEventListener('click', showElement)
    }
    toggleVisibility('homeNav')

    document.getElementById('searchBar').addEventListener('input', searchInTable)
    document.getElementById('submitButton').addEventListener('click', createAttractiePark)
}
