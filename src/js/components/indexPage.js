import{ATTRACTIEPARK_URL} from '../variabelen'
async function makeCards()
{

    const cards = await getSomeAttractieparken()
    const BASE_URL  = 'http://localhost:3000/'
    cards.forEach(attractiePark => {
        const div = document.createElement('div')
        div.classList.add('card')
        
        div.innerHTML = `<img class="card-img-top" src="${BASE_URL}${attractiePark.image}">
            <div class="card-body"><h4 class="card-title">${attractiePark.name}</h1>
            <p class="card-text">Locatie: ${attractiePark.location}
            <br>Opening: ${attractiePark.dateFounded}</p></div>`
        document.getElementById('few').append(div)
    }) 
    console.log('test'+cards) 
}

async function getSomeAttractieparken(){
    const response = await fetch(ATTRACTIEPARK_URL)

    const json = await response.json()
  
    const someAttractiepark = await json.filter(at => at.id <= 6)
    console.log(someAttractiepark)

    return someAttractiepark
}
export {makeCards, getSomeAttractieparken}