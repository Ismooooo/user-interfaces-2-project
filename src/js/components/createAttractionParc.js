import {postAttractiepark} from '../restclient'
import {getAttractieparken} from '../variabelen'

export function createAttractiePark(){
    let attractieParkCount = getAttractieparken().length
   
    let attractiePark = {
        id:++attractieParkCount,
        name: document.getElementById('naam').value,
        location: document.getElementById('locatie').value,
        dateFounded: document.getElementById('openingsDatum').value,
        annualAttendance: parseInt(document.getElementById('bezoekers').value)
    }
    postAttractiepark(attractiePark)
}