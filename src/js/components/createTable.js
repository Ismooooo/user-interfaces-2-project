import {getAttractieparken} from '../variabelen.js'

let attractieParken

async function showTable() {
    attractieParken = await getAttractieparken()
    let tableBody = document.getElementById('tableBody')
    tableBody.innerHTML=''
    attractieParken.forEach(attractiePark => {
        let newRow = document.createElement('tr')
        newRow.classList.add('show','table-row')
        newRow.innerHTML = `
        <td scope="row"><div>${attractiePark.id}</div></td>
        <td><div>${attractiePark.name}</div></td>
        <td><div>${attractiePark.location}</div></td>
        <td><div>${attractiePark.dateFounded}</div></td>
        <td><div>${attractiePark.annualAttendance}</div></td>`

        newRow.id =  `table-${attractiePark.id}`
        tableBody.appendChild(newRow)

    })
}

async function searchInTable(element)
{
    let elementFilter = element.target.value.toLowerCase()
    let filteredTable 
    attractieParken =await getAttractieparken()
    if(elementFilter!==''){
        filteredTable = attractieParken.filter(
            attractiePark => attractiePark.location.toLowerCase().includes(elementFilter) 
        )
        console.log(filteredTable)
        showFitlerTable(filteredTable)
        console.log(elementFilter)

    }else{filteredTable = attractieParken
        showTable()}
  
    async function showFitlerTable(attractieParken) {
       
        let tableBody = document.getElementById('tableBody')
        tableBody.innerHTML=''
        attractieParken.forEach(attractiePark => {
            let newRow = document.createElement('tr')
            newRow.classList.add('show','table-row')
            newRow.innerHTML = `
            <td scope="row"><div>${attractiePark.id}</div></td>
            <td><div>${attractiePark.name}</div></td>
            <td><div>${attractiePark.location}</div></td>
            <td><div>${attractiePark.dateFounded}</div></td>
            <td><div>${attractiePark.annualAttendance}</div></td>`
    
            newRow.id =  `table-${attractiePark.id}`
            tableBody.appendChild(newRow)
    
        })
    }
}
export { searchInTable, showTable }
