import {ATTRACTIEPARK_URL} from './variabelen'

export async function fetchAttractieparken () {   
    /* await fetch(ATTRACTIEPARK_URL)
        .then(function (response) {
            if (!response.ok) {
                throw Error(
                    'Ik kon de attractiepark niet ophalen: ' +
            response.status +
            ' ' +
            response.statusText
                )
            }
            return response.json()
        })
        .catch(function (e) {
            console.error(e)
            throw Error(e)
        })*/
    const fullURL = ATTRACTIEPARK_URL
    const response = await fetch(fullURL)
    return response.json()
}

export function postAttractiepark(attractiepark){
    let settings = {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(attractiepark)
    }
    console.log(attractiepark)
    let response =  fetch(`${ATTRACTIEPARK_URL}`, settings)
    console.log(response)
}
