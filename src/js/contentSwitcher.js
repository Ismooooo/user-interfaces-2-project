import { showTable } from './components/createTable'
import { navigation, sections } from './variabelen.js'
let navIdArray = []
let contentIdArray = []
for (let element of sections) {
    contentIdArray.push(element.id)
}
for (let element of navigation) {
    navIdArray.push(element.id)
}

function showElement({ target }) {
    let targetDoC = document.getElementById(target.id)
    console.log(target.id)
    switch (target.id) {
    case 'homeNav':
        targetDoC.addEventListener('click', toggleVisibility(contentIdArray[0]))
        break
    case 'newNav':
        targetDoC.addEventListener('click', toggleVisibility(contentIdArray[1]))
        break
    case 'searchNav':
        targetDoC.addEventListener('click', toggleVisibility(contentIdArray[2]))
        showTable()
        break
    }
}

var visibleDivId = null
export function toggleVisibility(divId) {
    if (visibleDivId === divId) {
        visibleDivId = null
    } else {
        visibleDivId = divId
    }
    hideNonVisibleDivs()
}
function hideNonVisibleDivs() {
    var i, divId, div
    for (i = 0; i < contentIdArray.length; i++) {
        divId = contentIdArray[i]
        div = document.getElementById(divId)
        if (visibleDivId === divId) {
            div.style.display = 'block'
        } else {
            div.style.display = 'none'
        }
    }
}
export { showElement }
