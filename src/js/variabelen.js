import{fetchAttractieparken} from './restclient.js'


const ATTRACTIEPARK_URL = 'http://localhost:3000/attractieParken/'

let navigation = document.getElementsByClassName('nav-link')
let sections = document.getElementsByClassName('content')

let attractieparken 
async function getAttractieparken() {
    if(attractieparken==null)
    {
        let attractieparkenArray = await fetchAttractieparken()
        console.log(attractieparkenArray)
        attractieparken =  attractieparkenArray.sort((a,b)=>a.id<b.id)

        console.log(attractieparken)
    } 

    return attractieparken
}

export{
    getAttractieparken,
    navigation,
    ATTRACTIEPARK_URL,
    sections
}