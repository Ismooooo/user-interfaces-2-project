class Attractiepark
{
    constructor(id,name, location, dateFounded, annualAttendance)
    {
        this.id = id
        this.name = name
        this.location = location
        this.dateFounded = dateFounded
        this.annualAttendance = annualAttendance
    }
}

export default Attractiepark